//
//  MatchListView.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollapseClick.h"

@interface MatchListView : UIView<CollapseClickDelegate, UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIView *InProgressMatchesView;
    IBOutlet UITableView *inProgressTable;
    NSMutableArray *inProressMatchesArray;
    
    IBOutlet UIView *CompletedMatchesView;
    IBOutlet UITableView *completedTable;
    NSMutableArray *completedMatchesArray;

    IBOutlet CollapseClick *myCollapseClick;
}

@property(nonatomic) UIView *InProgressMatchesView;
@property(nonatomic) UITableView *inProgressTable;
@property(nonatomic) NSMutableArray *inProressMatchesArray;

@property(nonatomic) UIView *CompletedMatchesView;
@property(nonatomic) UITableView *completedTable;
@property(nonatomic) NSMutableArray *completedMatchesArray;

@property(nonatomic) CollapseClick *myCollapseClick;


+ (id)getMatchListView;

-(void)setupMatchListView;

@end
