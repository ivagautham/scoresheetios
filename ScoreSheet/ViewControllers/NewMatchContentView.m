//
//  NewMatchContentView.m
//  ScoreSheet
//
//  Created by VA Gautham  on 7-5-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "NewMatchContentView.h"

@implementation NewMatchContentView
@synthesize timeBtn, timeTextBtn, locationBtn, locationTextField;
@synthesize teamATextField,teamBTextField,teamATossBtn,teamBTossBtn,teamADecisionBtn,teamBDecisionBtn;
@synthesize matchDetaisView,formatLabel,formatSegment,oversLabel,oversBtn,inningsLabel,inningsSegment,umpiresLabel,umpiresCountLabel,addUmpiresBtn;
@synthesize datePickerView,datePicker,datePickerCancelBtn,datePickerDoneBtn;
@synthesize pickerView,picker,pickerCancelBtn,pickerDoneBtn;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (id)getNewMatchContentView
{
    NewMatchContentView *customView;
    NSArray *viewArray = [[NSBundle mainBundle] loadNibNamed:@"NewMatchContentView" owner:nil options:nil];
    
    // make sure customView is not nil or the wrong class!
    for (customView in viewArray)
        return customView;
    
    return nil;
}

-(void)setupewMatchContentView
{
    NSString *currentTime = [ScoreSheetUtils getCurrentTime];
    [timeTextBtn setTitle:currentTime forState:UIControlStateNormal];
    
}

-(IBAction)onTimeBtn:(id)sender
{
    if (datePickerView.hidden)
    {
        datePickerView.hidden = FALSE;
    }
    else
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy MMM dd, HH:mm"];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        NSString *currentTime = [dateFormatter stringFromDate:datePicker.date];
        
        [timeTextBtn setTitle:currentTime forState:UIControlStateNormal];
        datePickerView.hidden = TRUE;
    }
}

-(IBAction)onDatePickerDone:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy MMM dd, HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *currentTime = [dateFormatter stringFromDate:datePicker.date];
    
    [timeTextBtn setTitle:currentTime forState:UIControlStateNormal];
    datePickerView.hidden = TRUE;
}

-(IBAction)onDatePickerCancel:(id)sender
{
    datePickerView.hidden = FALSE;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
