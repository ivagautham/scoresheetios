//
//  MatchListViewController.m
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "MatchListViewController.h"

@interface MatchListViewController ()

@end

@implementation MatchListViewController
@synthesize mMatchListView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    nibNameOrNil = @"MatchListViewController-iPhone";
    mMatchListView = nil;
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mMatchListView = [MatchListView getMatchListView];
        [mMatchListView setupMatchListView];
        [mMatchListView setFrame:[[self view] bounds]];
        [self.view addSubview:mMatchListView];
        [self.view bringSubviewToFront:mMatchListView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[ScoreSheetUtils getBackgroundColor]];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = YES;
    [mMatchListView.myCollapseClick scrollsToTop];
    [self setUpNavigationBar];
}

-(void)setUpNavigationBar
{
    [self.navigationController setNavigationBarHidden:FALSE];
    
    [self.navigationController.navigationBar setTintColor:[UIColor darkGrayColor]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_ScoreSheet"]];
    
    [self.navigationItem setHidesBackButton:FALSE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
