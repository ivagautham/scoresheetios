//
//  HomeViewController.h
//  ScoreSheet
//
//  Created by Gautham on 10/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate>
{
    IBOutlet UIView *dashboardView;
    
    IBOutlet UIButton *newMatchBtn;
    IBOutlet UIButton *inProgressBtn;
    IBOutlet UIButton *completedMatchesBtn;
    IBOutlet UIButton *statsBtn;
    
    IBOutlet UIButton *feedbackBtn;
    IBOutlet UIButton *rateBtn;
    IBOutlet UIButton *shareBtn;
    IBOutlet UIButton *helpBtn;
    IBOutlet UIButton *aboutBtn;
    
    IBOutlet UICollectionView *dashboardCollectionView;
    NSMutableArray *allMatchesArray;
    IBOutlet UIPageControl *collectionViewPageControl;
}

@property(nonatomic, strong) UIView *dashboardView;
@property(nonatomic, strong) UIButton *newMatchBtn;
@property(nonatomic, strong) UIButton *inProgressBtn;
@property(nonatomic, strong) UIButton *completedMatchesBtn;
@property(nonatomic, strong) UIButton *statsBtn;

@property(nonatomic, strong) UIButton *feedbackBtn;
@property(nonatomic, strong) UIButton *rateBtn;
@property(nonatomic, strong) UIButton *shareBtn;
@property(nonatomic, strong) UIButton *helpBtn;
@property(nonatomic, strong) UIButton *aboutBtn;

@property(nonatomic, strong) UICollectionView *dashboardCollectionView;
@property(nonatomic, strong) NSMutableArray *allMatchesArray;
@property(nonatomic, strong) UIPageControl *collectionViewPageControl;

-(IBAction)onNewMatchBtn:(id)sender;
-(IBAction)onInProgressBtn:(id)sender;
-(IBAction)onCompletedMatchesBtn:(id)sender;
-(IBAction)onStatsBtn:(id)sender;

-(IBAction)onFeedbackBtn:(id)sender;
-(IBAction)onRateBtn:(id)sender;
-(IBAction)onShareBtn:(id)sender;
-(IBAction)onHelpBtn:(id)sender;
-(IBAction)onAboutBtn:(id)sender;

@end
