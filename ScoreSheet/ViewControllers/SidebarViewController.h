//
//  SidebarViewController.h
//  ScoreSheet
//
//  Created by VA Gautham  on 2-5-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidebarViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *sidebarTable;
}

@property(nonatomic) UITableView *sidebarTable;

@end
