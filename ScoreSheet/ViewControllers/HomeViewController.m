//
//  HomeViewController.m
//  ScoreSheet
//
//  Created by Gautham on 10/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "HomeViewController.h"
#import "MatchListViewController.h"
#import "MatchListView.h"
#import "SWRevealViewController.h"
#import "NewMatchCollectionViewCell.h"
#import "NewMatchViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@dynamic newMatchBtn, inProgressBtn, completedMatchesBtn, statsBtn;
@synthesize feedbackBtn, rateBtn, shareBtn, aboutBtn, helpBtn;
@synthesize dashboardCollectionView, allMatchesArray, collectionViewPageControl;
@synthesize dashboardView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (IS_IPAD)
        nibNameOrNil = @"HomeViewController-iPad";
    else
        nibNameOrNil = @"HomeViewController-iPhone";
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if (IS_IPAD) {
            MatchListView *mMatchListView = [MatchListView getMatchListView];
            [mMatchListView setupMatchListView];
            CGRect matchListViewFrame = CGRectMake(0, 64, 413, 696);
            [mMatchListView setFrame:matchListViewFrame];
            [self.view addSubview:mMatchListView];
            [self.view bringSubviewToFront:mMatchListView];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[ScoreSheetUtils getBackgroundColor]];
    
    newMatchBtn.layer.borderWidth = 0.25;
    statsBtn.layer.borderWidth = 0.25;
    
    newMatchBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
    statsBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
   
    if (!IS_IPAD)
    {
        inProgressBtn.layer.borderWidth = 0.25;
        completedMatchesBtn.layer.borderWidth = 0.25;
        
        inProgressBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
        completedMatchesBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    else
    {
        newMatchBtn.layer.borderWidth = 0.3;
        statsBtn.layer.borderWidth = 0.3;
        
        feedbackBtn.layer.borderWidth = 0.3;
        rateBtn.layer.borderWidth = 0.3;
        shareBtn.layer.borderWidth = 0.3;
        aboutBtn.layer.borderWidth = 0.3;
        helpBtn.layer.borderWidth = 0.3;

        feedbackBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
        rateBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
        shareBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
        aboutBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
        helpBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
    }
    
    [dashboardCollectionView setBackgroundColor:[UIColor clearColor]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setUpNavigationBar];
    
    if (!allMatchesArray)
        allMatchesArray = [[NSMutableArray alloc] init];
    
    [allMatchesArray removeAllObjects]; //To add matches from coredata
    dashboardCollectionView.delegate = self;
    dashboardCollectionView.dataSource = self;
    [dashboardCollectionView reloadData];
}

-(void)setUpNavigationBar
{
    [self.navigationController setNavigationBarHidden:FALSE];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_ScoreSheet"]];
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    if (!IS_IPAD)
    {
        
        SWRevealViewController *revealController = [self revealViewController];
        
        [revealController panGestureRecognizer];
        [revealController tapGestureRecognizer];
        
        
        UIBarButtonItem* leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"]
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self.revealViewController
                                                                         action:@selector(revealToggle:)];
        [leftBarButton setTintColor:[ScoreSheetUtils getPatchColor]];
        [self.navigationItem setLeftBarButtonItem:leftBarButton];
        
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    UIBarButtonItem* rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_newMatch"]
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(onNewMatchBtn:)];
    [rightBarButton setTintColor:[ScoreSheetUtils getPatchColor]];
    [self.navigationItem setRightBarButtonItem:rightBarButton];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onNewMatchBtn:(id)sender
{
    BOOL isMatchInProgress = FALSE; //To check if any match is in progress
    if (isMatchInProgress)
    {
        UIAlertView *matchInProgressAlert = [[UIAlertView alloc] initWithTitle:@"Match already in Progress" message:@"Few matches already in progress \n Are you sure you want to create a new match?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        
        matchInProgressAlert.tag = ALERT_NEW_MATCH;
        matchInProgressAlert.delegate = self;
        [matchInProgressAlert show];
    }
    else
    {
        NewMatchViewController *mNewMatchViewController = [[NewMatchViewController alloc] initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:mNewMatchViewController animated:TRUE];
    }
}

-(IBAction)onInProgressBtn:(id)sender
{
    MatchListViewController *mMatchListViewController = [[MatchListViewController alloc] initWithNibName:nil bundle:nil];
    [mMatchListViewController.mMatchListView.myCollapseClick openCollapseClickCellAtIndex:0 animated:NO];
    [self.navigationController pushViewController:mMatchListViewController animated:TRUE];
}

-(IBAction)onCompletedMatchesBtn:(id)sender
{
    MatchListViewController *mMatchListViewController = [[MatchListViewController alloc] initWithNibName:nil bundle:nil];
    [mMatchListViewController.mMatchListView.myCollapseClick openCollapseClickCellAtIndex:1 animated:NO];
    [self.navigationController pushViewController:mMatchListViewController animated:TRUE];
}

-(IBAction)onStatsBtn:(id)sender
{
    
}

-(IBAction)onFeedbackBtn:(id)sender
{
    
}

-(IBAction)onRateBtn:(id)sender
{
    
}

-(IBAction)onShareBtn:(id)sender
{
    
}

-(IBAction)onHelpBtn:(id)sender
{
    
}

-(IBAction)onAboutBtn:(id)sender
{
    
}

#pragma mark - UICollectionView Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger rowCount = 0;
    if ([allMatchesArray count] == 0)
        rowCount =  1;
    else
        rowCount =  allMatchesArray.count;
    
    collectionViewPageControl.numberOfPages = rowCount;
    collectionViewPageControl.currentPage = 0;
    return rowCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([allMatchesArray count] == 0)
    {
        static NSString * CellIdentifier = @"NewMatchCollectionViewCell";
        static BOOL nibMyCellloaded = NO;
        if(!nibMyCellloaded)
        {
            UINib *nib = [UINib nibWithNibName:CellIdentifier bundle: nil];
            [dashboardCollectionView registerNib:nib forCellWithReuseIdentifier:CellIdentifier];
            nibMyCellloaded = YES;
        }

        NewMatchCollectionViewCell * cell = (NewMatchCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        if (!cell)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewMatchCollectionViewCell" owner:self options:nil];
            cell = (NewMatchCollectionViewCell *)[nib objectAtIndex:0];
        }
        
        return cell;
    }
    else
    {
        
    }
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    if ([allMatchesArray count] == 0)
    {
        [self onNewMatchBtn:nil];
    }
    else
    {
        
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = dashboardCollectionView.frame.size.width;
    collectionViewPageControl.currentPage = dashboardCollectionView.contentOffset.x / pageWidth;
}

#pragma mark - UIAlertView Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == ALERT_NEW_MATCH)
    {
        if (buttonIndex == 1)
        {
            NewMatchViewController *mNewMatchViewController = [[NewMatchViewController alloc] initWithNibName:nil bundle:nil];
            [self.navigationController pushViewController:mNewMatchViewController animated:TRUE];
        }
    }
}

@end
