//
//  NewMatchViewController.h
//  ScoreSheet
//
//  Created by VA Gautham  on 7-5-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMatchContentView.h"

@interface NewMatchViewController : UIViewController<UIAlertViewDelegate>
{
    NewMatchContentView *mNewMatchContentView;
}

@property(nonatomic) NewMatchContentView *mNewMatchContentView;

@end
