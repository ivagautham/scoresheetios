//
//  SidebarViewController.m
//  ScoreSheet
//
//  Created by VA Gautham  on 2-5-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "SidebarViewController.h"
#import "SWRevealViewController.h"
#import "SidebarCell.h"

@interface SidebarViewController ()
{
    NSArray *menuItems;
}
@end

@implementation SidebarViewController
@synthesize sidebarTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    nibNameOrNil = @"SidebarViewController";

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController setNavigationBarHidden:TRUE];

    menuItems = @[@"Feedback", @"Rate Our App", @"Share The App", @"Help", @"About"];
    sidebarTable.delegate = self;
    sidebarTable.dataSource = self;
    [sidebarTable reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"SidebarCell";
    SidebarCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SidebarCell" owner:self options:nil];
        cell = (SidebarCell *)[nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.contentLabel.text = [menuItems objectAtIndex:indexPath.row];
    cell.contentImage.image = [self getImageForCellIndex:indexPath.row];
    
    return cell;
}

-(UIImage *)getImageForCellIndex:(NSUInteger)index
{
    UIImage *cellImage = nil;
    switch (index) {
        case 0:
            cellImage = [UIImage imageNamed:@"btn_feedback"];
            break;
        case 1:
            cellImage = [UIImage imageNamed:@"btn_rate"];
            break;
        case 2:
            cellImage = [UIImage imageNamed:@"btn_share"];
            break;
        case 3:
            cellImage = [UIImage imageNamed:@"btn_help"];
            break;
        case 4:
            cellImage = [UIImage imageNamed:@"btn_about"];
            break;
            
        default:
            break;
    }
    return cellImage;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
