//
//  SplashViewController.m
//  ScoreSheet
//
//  Created by Gautham on 10/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "SplashViewController.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "SidebarViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (IS_IPAD)
        nibNameOrNil = @"SplashViewController-iPad";
    else
        nibNameOrNil = @"SplashViewController-iPhone";
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[ScoreSheetUtils getSplashBackground]];
    double delayInSeconds = 2.25;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        HomeViewController *mHomeViewController = [[HomeViewController alloc] initWithNibName:nil bundle:nil];
        if (IS_IPAD)
        {
            [self.navigationController pushViewController:mHomeViewController animated:YES];
        }
        else
        {
            SidebarViewController *mSidebarViewController = [[SidebarViewController alloc] initWithNibName:nil bundle:nil];
            
            UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:mHomeViewController];
            UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:mSidebarViewController];
            
            SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
            
            [self.navigationController pushViewController:revealController animated:YES];
        }
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.navigationController setNavigationBarHidden:TRUE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
