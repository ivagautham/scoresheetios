//
//  MatchListView.m
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "MatchListView.h"
#import "MatchListCell.h"
#import "MatchListEmptyCell.h"

@implementation MatchListView
@synthesize InProgressMatchesView, inProgressTable, inProressMatchesArray;
@synthesize CompletedMatchesView, completedTable, completedMatchesArray;
@synthesize myCollapseClick;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (id)getMatchListView
{
    MatchListView *customView;
    NSArray *viewArray = [[NSBundle mainBundle] loadNibNamed:@"MatchListView" owner:nil options:nil];
    
    // make sure customView is not nil or the wrong class!
    for (customView in viewArray)
        return customView;
    
    return nil;
}

-(void)setupMatchListView
{
    if (!inProressMatchesArray)
        inProressMatchesArray = [[NSMutableArray alloc] init];
    
    if (!completedMatchesArray)
        completedMatchesArray = [[NSMutableArray alloc] init];

    [inProressMatchesArray removeAllObjects]; //To add matches from coredata
    [completedMatchesArray removeAllObjects];
    [completedMatchesArray addObject:@"temp"];
    [completedMatchesArray addObject:@"temp2"];
    [completedMatchesArray addObject:@"temp3"];
    [completedMatchesArray addObject:@"temp4"];

    inProgressTable.delegate = self;
    inProgressTable.dataSource = self;
    [inProgressTable reloadData];
    
    completedTable.delegate = self;
    completedTable.dataSource = self;
    [completedTable reloadData];
   
    myCollapseClick.CollapseClickDelegate = self;
    [myCollapseClick reloadCollapseClick];
    
}

#pragma mark - Collapse Click Delegate

// Required Methods
-(int)numberOfCellsForCollapseClick
{
    return 2;
}

-(NSString *)titleForCollapseClickAtIndex:(int)index
{
    switch (index)
    {
        case 0:
            return [NSString stringWithFormat:@"In Progress Matches (%lu)",(unsigned long)inProressMatchesArray.count];
            break;
        case 1:
            return [NSString stringWithFormat:@"Completed Matches (%lu)",(unsigned long)completedMatchesArray.count];
            break;
            
        default:
            return @"";
            break;
    }
}

-(UIView *)viewForCollapseClickContentViewAtIndex:(int)index
{
    switch (index) {
        case 0:
            return InProgressMatchesView;
            break;
        case 1:
            return CompletedMatchesView;
            break;
            
        default:
            return InProgressMatchesView;
            break;
    }
}


// Optional Methods

-(UIColor *)colorForCollapseClickTitleViewAtIndex:(int)index
{
    return [UIColor whiteColor];
}

-(UIColor *)colorForTitleLabelAtIndex:(int)index
{
    return [UIColor lightGrayColor];
}

-(UIColor *)colorForTitleArrowAtIndex:(int)index
{
    return [UIColor greenColor];
}

#pragma mark - TableView Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowCount = 0;
    if (tableView == inProgressTable)
    {
        if ([inProressMatchesArray count] == 0)
            rowCount = 1;
        else
            rowCount = inProressMatchesArray.count;
        
        CGRect rect = inProgressTable.frame;
        rect.size.height = rowCount * 120;
        if (rect.size.height == 0)
            rect.size.height = 120;
        
        inProgressTable.frame = rect;
        InProgressMatchesView.frame = rect;
    }
    
    if (tableView == completedTable)
    {
        if ([completedMatchesArray count] == 0)
            rowCount = 1;
        else
            rowCount = completedMatchesArray.count;
        
        CGRect rect = completedTable.frame;
        rect.size.height = rowCount * 120;
        if (rect.size.height == 0)
            rect.size.height = 120;
        
        completedTable.frame = rect;
        CompletedMatchesView.frame = rect;
    }
    
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == inProgressTable)
    {
        if ([inProressMatchesArray count] == 0)
        {
            static NSString * CellIdentifier = @"MatchListEmptyCell";
            MatchListEmptyCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MatchListEmptyCell" owner:self options:nil];
                cell = (MatchListEmptyCell *)[nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.infoLabel.text = @"There are no matches in progress at the moment.";
            return cell;
        }
        else
        {
            static NSString * CellIdentifier = @"MatchListCell";
            MatchListCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MatchListCell" owner:self options:nil];
                cell = (MatchListCell *)[nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            if (indexPath.row % 2 == 0)
                [cell setBackgroundColor:[UIColor whiteColor]];
            else
                [cell setBackgroundColor:[UIColor colorWithRed:(240.0/255.0) green:(240.0/255.0) blue:(240.0/255.0) alpha:1.0]];

            return cell;
        }
    }
    
    if (tableView == completedTable)
    {
        if ([completedMatchesArray count] == 0)
        {
            static NSString * CellIdentifier = @"MatchListEmptyCell";
            MatchListEmptyCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MatchListEmptyCell" owner:self options:nil];
                cell = (MatchListEmptyCell *)[nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.infoLabel.text = @"There are no completed matches results at the moment.";
            return cell;
        }
        else
        {
            static NSString * CellIdentifier = @"MatchListCell";
            MatchListCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MatchListCell" owner:self options:nil];
                cell = (MatchListCell *)[nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
           
            if (indexPath.row % 2 == 0)
                [cell setBackgroundColor:[UIColor whiteColor]];
            else
                [cell setBackgroundColor:[UIColor colorWithRed:(240.0/255.0) green:(240.0/255.0) blue:(240.0/255.0) alpha:1.0]];
            
            return cell;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
