//
//  MatchListViewController.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatchListView.h"

@interface MatchListViewController : UIViewController
{
    MatchListView *mMatchListView;
}

@property(nonatomic) MatchListView *mMatchListView;

@end
