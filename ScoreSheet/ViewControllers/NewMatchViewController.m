//
//  NewMatchViewController.m
//  ScoreSheet
//
//  Created by VA Gautham  on 7-5-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "NewMatchViewController.h"

@interface NewMatchViewController ()

@end

@implementation NewMatchViewController
@synthesize mNewMatchContentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (IS_IPAD)
        nibNameOrNil = @"NewMatchViewController-iPad";
    else
        nibNameOrNil = @"NewMatchViewController-iPhone";

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mNewMatchContentView = [NewMatchContentView getNewMatchContentView];
        [mNewMatchContentView setupewMatchContentView];
        CGRect frame = [[self view] bounds];
        frame.origin.y = frame.origin.y + 66;
        frame.size.height = frame.size.height - 66;
        [mNewMatchContentView setFrame:frame];
        [self.view addSubview:mNewMatchContentView];
        [self.view bringSubviewToFront:mNewMatchContentView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[ScoreSheetUtils getBackgroundColor]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setUpNavigationBar];
}

-(void)setUpNavigationBar
{
    [self.navigationController setNavigationBarHidden:FALSE];
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    self.navigationItem.title = @"New Match";
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    UIBarButtonItem* leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ico_discard"]
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(onDiscardBtn)];
    [leftBarButton setTintColor:[ScoreSheetUtils getPatchColor]];
    [self.navigationItem setLeftBarButtonItem:leftBarButton];

}

-(void)onDiscardBtn
{
    UIAlertView *discardMatchAlert = [[UIAlertView alloc] initWithTitle:@"Discard Match" message:@"Are you sure want to discard this match?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    discardMatchAlert.tag = ALERT_DISCARD_MATCH;
    discardMatchAlert.delegate = self;
    [discardMatchAlert show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIAlertView Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == ALERT_DISCARD_MATCH)
    {
        if (buttonIndex == 1)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
@end
