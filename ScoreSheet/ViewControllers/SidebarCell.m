//
//  SidebarCell.m
//  ScoreSheet
//
//  Created by VA Gautham  on 2-5-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "SidebarCell.h"

@implementation SidebarCell
@synthesize contentLabel, contentImage;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
