//
//  SidebarCell.h
//  ScoreSheet
//
//  Created by VA Gautham  on 2-5-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidebarCell : UITableViewCell
{
    IBOutlet UIImageView *contentImage;
    IBOutlet UILabel *contentLabel;
}

@property(nonatomic) UIImageView *contentImage;
@property(nonatomic) UILabel *contentLabel;

@end
