//
//  MatchListEmptyCell.h
//  ScoreSheet
//
//  Created by VA Gautham  on 30-4-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchListEmptyCell : UITableViewCell
{
    IBOutlet UILabel *infoLabel;
}

@property(nonatomic) UILabel *infoLabel;

@end
