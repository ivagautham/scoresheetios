//
//  MatchListCell.m
//  ScoreSheet
//
//  Created by VA Gautham  on 30-4-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "MatchListCell.h"

@implementation MatchListCell
@synthesize matchTitle;
@synthesize matchStatus;
@synthesize team1Label;
@synthesize team2Label;
@synthesize team1FirstInnings;
@synthesize team1secongInnings;
@synthesize team2FirstInnings;
@synthesize team2secongInnings;
@synthesize team1Logo;
@synthesize team2Logo;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
