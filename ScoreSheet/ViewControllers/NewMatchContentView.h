//
//  NewMatchContentView.h
//  ScoreSheet
//
//  Created by VA Gautham  on 7-5-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMatchContentView : UIView
{
    IBOutlet UIButton *timeBtn;
    IBOutlet UIButton *timeTextBtn;
    IBOutlet UIButton *locationBtn;
    IBOutlet UITextField *locationTextField;
    
    IBOutlet UITextField *teamATextField;
    IBOutlet UITextField *teamBTextField;
    IBOutlet UIButton *teamATossBtn;
    IBOutlet UIButton *teamBTossBtn;
    IBOutlet UIButton *teamADecisionBtn;
    IBOutlet UIButton *teamBDecisionBtn;
    
    IBOutlet UIView *matchDetaisView;
    IBOutlet UILabel *formatLabel;
    IBOutlet UISegmentedControl *formatSegment;
    IBOutlet UILabel *oversLabel;
    IBOutlet UIButton *oversBtn;
    IBOutlet UILabel *inningsLabel;
    IBOutlet UISegmentedControl *inningsSegment;
    IBOutlet UILabel *umpiresLabel;
    IBOutlet UILabel *umpiresCountLabel;
    IBOutlet UIButton *addUmpiresBtn;
    
    IBOutlet UIView *datePickerView;
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIButton *datePickerCancelBtn;
    IBOutlet UIButton *datePickerDoneBtn;
    
    IBOutlet UIView *pickerView;
    IBOutlet UIPickerView *picker;
    IBOutlet UIButton *pickerCancelBtn;
    IBOutlet UIButton *pickerDoneBtn;
}

@property(nonatomic) UIButton *timeBtn;
@property(nonatomic) UIButton *timeTextBtn;
@property(nonatomic) UIButton *locationBtn;
@property(nonatomic) UITextField *locationTextField;

@property(nonatomic) UITextField *teamATextField;
@property(nonatomic) UITextField *teamBTextField;
@property(nonatomic) UIButton *teamATossBtn;
@property(nonatomic) UIButton *teamBTossBtn;
@property(nonatomic) UIButton *teamADecisionBtn;
@property(nonatomic) UIButton *teamBDecisionBtn;

@property(nonatomic) UIView *matchDetaisView;
@property(nonatomic) UILabel *formatLabel;
@property(nonatomic) UISegmentedControl *formatSegment;
@property(nonatomic) UILabel *oversLabel;
@property(nonatomic) UIButton *oversBtn;
@property(nonatomic) UILabel *inningsLabel;
@property(nonatomic) UISegmentedControl *inningsSegment;
@property(nonatomic) UILabel *umpiresLabel;
@property(nonatomic) UILabel *umpiresCountLabel;
@property(nonatomic) UIButton *addUmpiresBtn;

@property(nonatomic) UIView *datePickerView;
@property(nonatomic) UIDatePicker *datePicker;
@property(nonatomic) UIButton *datePickerCancelBtn;
@property(nonatomic) UIButton *datePickerDoneBtn;

@property(nonatomic) UIView *pickerView;
@property(nonatomic) UIPickerView *picker;
@property(nonatomic) UIButton *pickerCancelBtn;
@property(nonatomic) UIButton *pickerDoneBtn;

+ (id)getNewMatchContentView;
-(void)setupewMatchContentView;

-(IBAction)onTimeBtn:(id)sender;

-(IBAction)onDatePickerDone:(id)sender;
-(IBAction)onDatePickerCancel:(id)sender;

@end
