//
//  MatchListCell.h
//  ScoreSheet
//
//  Created by VA Gautham  on 30-4-14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchListCell : UITableViewCell
{
    IBOutlet UILabel *matchTitle;
    IBOutlet UILabel *matchStatus;
    IBOutlet UILabel *team1Label;
    IBOutlet UILabel *team2Label;
    IBOutlet UILabel *team1FirstInnings;
    IBOutlet UILabel *team1secongInnings;
    IBOutlet UILabel *team2FirstInnings;
    IBOutlet UILabel *team2secongInnings;
    IBOutlet UIImageView *team1Logo;
    IBOutlet UIImageView *team2Logo;
}

@property(nonatomic) UILabel *matchTitle;
@property(nonatomic) UILabel *matchStatus;
@property(nonatomic) UILabel *team1Label;
@property(nonatomic) UILabel *team2Label;
@property(nonatomic) UILabel *team1FirstInnings;
@property(nonatomic) UILabel *team1secongInnings;
@property(nonatomic) UILabel *team2FirstInnings;
@property(nonatomic) UILabel *team2secongInnings;
@property(nonatomic) UIImageView *team1Logo;
@property(nonatomic) UIImageView *team2Logo;

@end
