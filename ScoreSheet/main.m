//
//  main.m
//  ScoreSheet
//
//  Created by Gautham on 10/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
