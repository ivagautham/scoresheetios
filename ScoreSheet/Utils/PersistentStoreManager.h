//
//  PersistentStoreManager.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface PersistentStoreManager : NSObject
{
    NSManagedObjectContext * mainContext;
    NSPersistentStoreCoordinator * persistentStoreCoordinator;
    NSManagedObjectModel * managedObjectModel;
    
    BOOL fixingCoreDataUpdate;
    
    NSString *modelName;
}

@property (nonatomic, strong, readonly) NSManagedObjectContext * mainContext;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator * persistentStoreCoordinator;
@property (nonatomic, strong, readonly) NSManagedObjectModel * managedObjectModel;
@property (nonatomic, strong) NSString *modelName;

- (void) resetCoreData;

@end
