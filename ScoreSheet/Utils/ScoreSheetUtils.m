//
//  ScoreSheetUtils.m
//  ScoreSheet
//
//  Created by Gautham on 10/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "ScoreSheetUtils.h"

@implementation ScoreSheetUtils

+(UIColor *)getBackgroundColor
{
    NSString *imageName = nil;
    
    if (IS_IPHONE_5)
        imageName = @"bg_ScoreSheet-568.jpg";
    else if (IS_IPHONE)
        imageName = @"bg_ScoreSheet.jpg";
    else if (IS_IPAD)
        imageName = @"bg_ScoreSheet-Landscape~iPad.jpg";
    
    return [UIColor colorWithPatternImage:[UIImage imageNamed:imageName]];
}

+(UIColor *)getSplashBackground
{
    NSString *imageName = nil;
    
    if (IS_IPHONE_5)
        imageName = @"Default-568.png";
    else if (IS_IPHONE)
        imageName = @"Default.png";
    else if (IS_IPAD)
        imageName = @"Default-Landscape~iPad.png";
    
    return [UIColor colorWithPatternImage:[UIImage imageNamed:imageName]];
}

+(UIColor *)getPatchColor
{
    return [UIColor darkGrayColor];
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (NSString *)getCurrentTime;
{
    NSDate *now = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy MMM dd, HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *timeNow = [dateFormatter stringFromDate:now];
    NSLog(@"The Current Time is %@",timeNow);
    
    return timeNow;
}
@end
