//
//  ScoreSheetUtils.h
//  ScoreSheet
//
//  Created by Gautham on 10/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScoreSheetUtils : NSObject

+ (UIColor *)getBackgroundColor;
+ (UIColor *)getSplashBackground;
+ (UIColor *)getPatchColor;
+ (UIImage *)imageWithColor:(UIColor *)color;

+ (NSString *)getCurrentTime;

@end

