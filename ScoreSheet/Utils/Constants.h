//
//  Constants.h
//  ScoreSheet
//
//  Created by Gautham on 10/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#pragma mark - UIScreen Methods

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0)

#pragma mark - UIAlertView Tags

#define ALERT_NEW_MATCH 0
#define ALERT_DISCARD_MATCH 1