//
//  Batting.m
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "Batting.h"
#import "BattingCluster.h"
#import "Player.h"


@implementation Batting

@dynamic batting_TotalRuns;
@dynamic batting_TotalBalls;
@dynamic batting_SR;
@dynamic batting_4;
@dynamic batting_6;
@dynamic batting_HowOut;
@dynamic batting_Bowler;
@dynamic batting_Fielder;
@dynamic fromBattingCluster;
@dynamic toPlayer;

@end
