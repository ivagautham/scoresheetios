//
//  Team.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Match, Player, TeamCluster, TeamWickets;

@interface Team : NSManagedObject

@property (nonatomic, retain) NSString * team_Captain;
@property (nonatomic, retain) NSString * team_Name;
@property (nonatomic, retain) Match *toMatch;
@property (nonatomic, retain) NSSet *fromPlayer;
@property (nonatomic, retain) NSSet *fromTeamRuns;
@property (nonatomic, retain) NSSet *fromTeamWickets;
@property (nonatomic, retain) NSSet *fromTeamCluster;
@end

@interface Team (CoreDataGeneratedAccessors)

- (void)addFromPlayerObject:(Player *)value;
- (void)removeFromPlayerObject:(Player *)value;
- (void)addFromPlayer:(NSSet *)values;
- (void)removeFromPlayer:(NSSet *)values;

- (void)addFromTeamRunsObject:(NSManagedObject *)value;
- (void)removeFromTeamRunsObject:(NSManagedObject *)value;
- (void)addFromTeamRuns:(NSSet *)values;
- (void)removeFromTeamRuns:(NSSet *)values;

- (void)addFromTeamWicketsObject:(TeamWickets *)value;
- (void)removeFromTeamWicketsObject:(TeamWickets *)value;
- (void)addFromTeamWickets:(NSSet *)values;
- (void)removeFromTeamWickets:(NSSet *)values;

- (void)addFromTeamClusterObject:(TeamCluster *)value;
- (void)removeFromTeamClusterObject:(TeamCluster *)value;
- (void)addFromTeamCluster:(NSSet *)values;
- (void)removeFromTeamCluster:(NSSet *)values;

@end
