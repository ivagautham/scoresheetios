//
//  TeamRuns.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Team;

@interface TeamRuns : NSManagedObject

@property (nonatomic, retain) NSNumber * teamRuns_TotalRuns;
@property (nonatomic, retain) NSNumber * teamRuns_Extras;
@property (nonatomic, retain) Team *toTeam;

@end
