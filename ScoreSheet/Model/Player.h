//
//  Player.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Player : NSManagedObject

@property (nonatomic, retain) NSString * player_Name;
@property (nonatomic, retain) NSNumber * player_Role;
@property (nonatomic, retain) NSNumber * player_Number;
@property (nonatomic, retain) NSManagedObject *toTeam;
@property (nonatomic, retain) NSSet *fromBatting;
@property (nonatomic, retain) NSSet *fromBowling;
@property (nonatomic, retain) NSSet *fromCatches;
@property (nonatomic, retain) NSSet *fromStumpings;
@property (nonatomic, retain) NSSet *fromRunOuts;
@end

@interface Player (CoreDataGeneratedAccessors)

- (void)addFromBattingObject:(NSManagedObject *)value;
- (void)removeFromBattingObject:(NSManagedObject *)value;
- (void)addFromBatting:(NSSet *)values;
- (void)removeFromBatting:(NSSet *)values;

- (void)addFromBowlingObject:(NSManagedObject *)value;
- (void)removeFromBowlingObject:(NSManagedObject *)value;
- (void)addFromBowling:(NSSet *)values;
- (void)removeFromBowling:(NSSet *)values;

- (void)addFromCatchesObject:(NSManagedObject *)value;
- (void)removeFromCatchesObject:(NSManagedObject *)value;
- (void)addFromCatches:(NSSet *)values;
- (void)removeFromCatches:(NSSet *)values;

- (void)addFromStumpingsObject:(NSManagedObject *)value;
- (void)removeFromStumpingsObject:(NSManagedObject *)value;
- (void)addFromStumpings:(NSSet *)values;
- (void)removeFromStumpings:(NSSet *)values;

- (void)addFromRunOutsObject:(NSManagedObject *)value;
- (void)removeFromRunOutsObject:(NSManagedObject *)value;
- (void)addFromRunOuts:(NSSet *)values;
- (void)removeFromRunOuts:(NSSet *)values;

@end
