//
//  Bowling.m
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "Bowling.h"
#import "Player.h"


@implementation Bowling

@dynamic bowling_TotalOvers;
@dynamic bowling_Maidens;
@dynamic bowling_RunsConceded;
@dynamic bowling_TotalWickets;
@dynamic bowling_Econ;
@dynamic bowling_SR;
@dynamic bowling_Average;
@dynamic fromBowlingCluster;
@dynamic toPlayer;
@dynamic fromBowlingBastmanOut;

@end
