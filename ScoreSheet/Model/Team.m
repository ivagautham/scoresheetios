//
//  Team.m
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "Team.h"
#import "Match.h"
#import "Player.h"
#import "TeamCluster.h"
#import "TeamWickets.h"


@implementation Team

@dynamic team_Captain;
@dynamic team_Name;
@dynamic toMatch;
@dynamic fromPlayer;
@dynamic fromTeamRuns;
@dynamic fromTeamWickets;
@dynamic fromTeamCluster;

@end
