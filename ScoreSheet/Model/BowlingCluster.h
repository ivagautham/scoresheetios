//
//  BowlingCluster.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bowling;

@interface BowlingCluster : NSManagedObject

@property (nonatomic, retain) NSNumber * bowlingCluster_Ball;
@property (nonatomic, retain) NSNumber * bowlingCluster_Over;
@property (nonatomic, retain) NSString * bowlingCluster_Result;
@property (nonatomic, retain) Bowling *toBowling;

@end
