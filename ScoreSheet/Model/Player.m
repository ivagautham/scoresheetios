//
//  Player.m
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "Player.h"


@implementation Player

@dynamic player_Name;
@dynamic player_Role;
@dynamic player_Number;
@dynamic toTeam;
@dynamic fromBatting;
@dynamic fromBowling;
@dynamic fromCatches;
@dynamic fromStumpings;
@dynamic fromRunOuts;

@end
