//
//  Bowling.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Player;

@interface Bowling : NSManagedObject

@property (nonatomic, retain) NSNumber * bowling_TotalOvers;
@property (nonatomic, retain) NSNumber * bowling_Maidens;
@property (nonatomic, retain) NSNumber * bowling_RunsConceded;
@property (nonatomic, retain) NSNumber * bowling_TotalWickets;
@property (nonatomic, retain) NSNumber * bowling_Econ;
@property (nonatomic, retain) NSNumber * bowling_SR;
@property (nonatomic, retain) NSNumber * bowling_Average;
@property (nonatomic, retain) NSSet *fromBowlingCluster;
@property (nonatomic, retain) Player *toPlayer;
@property (nonatomic, retain) NSSet *fromBowlingBastmanOut;
@end

@interface Bowling (CoreDataGeneratedAccessors)

- (void)addFromBowlingClusterObject:(NSManagedObject *)value;
- (void)removeFromBowlingClusterObject:(NSManagedObject *)value;
- (void)addFromBowlingCluster:(NSSet *)values;
- (void)removeFromBowlingCluster:(NSSet *)values;

- (void)addFromBowlingBastmanOutObject:(NSManagedObject *)value;
- (void)removeFromBowlingBastmanOutObject:(NSManagedObject *)value;
- (void)addFromBowlingBastmanOut:(NSSet *)values;
- (void)removeFromBowlingBastmanOut:(NSSet *)values;

@end
