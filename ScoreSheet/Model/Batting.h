//
//  Batting.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BattingCluster, Player;

@interface Batting : NSManagedObject

@property (nonatomic, retain) NSNumber * batting_TotalRuns;
@property (nonatomic, retain) NSNumber * batting_TotalBalls;
@property (nonatomic, retain) NSNumber * batting_SR;
@property (nonatomic, retain) NSNumber * batting_4;
@property (nonatomic, retain) NSNumber * batting_6;
@property (nonatomic, retain) NSNumber * batting_HowOut;
@property (nonatomic, retain) NSString * batting_Bowler;
@property (nonatomic, retain) NSString * batting_Fielder;
@property (nonatomic, retain) NSSet *fromBattingCluster;
@property (nonatomic, retain) Player *toPlayer;
@end

@interface Batting (CoreDataGeneratedAccessors)

- (void)addFromBattingClusterObject:(BattingCluster *)value;
- (void)removeFromBattingClusterObject:(BattingCluster *)value;
- (void)addFromBattingCluster:(NSSet *)values;
- (void)removeFromBattingCluster:(NSSet *)values;

@end
