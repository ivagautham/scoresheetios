//
//  BattingCluster.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BattingCluster : NSManagedObject

@property (nonatomic, retain) NSString * battingCluster_Result;
@property (nonatomic, retain) NSManagedObject *toBatting;

@end
