//
//  Innings.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Match;

@interface Innings : NSManagedObject

@property (nonatomic, retain) NSString * innings_team;
@property (nonatomic, retain) Match *toMatch;

@end
