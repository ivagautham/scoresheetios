//
//  RunOuts.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Player;

@interface RunOuts : NSManagedObject

@property (nonatomic, retain) NSNumber * runOuts_TotalRunOuts;
@property (nonatomic, retain) Player *toPlayer;

@end
