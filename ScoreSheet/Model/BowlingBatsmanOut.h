//
//  BowlingBatsmanOut.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bowling;

@interface BowlingBatsmanOut : NSManagedObject

@property (nonatomic, retain) NSString * bowlingBatsmanOut_Name;
@property (nonatomic, retain) Bowling *toBowling;

@end
