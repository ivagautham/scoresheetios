//
//  Match.m
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "Match.h"
#import "Innings.h"
#import "Umpire.h"


@implementation Match

@dynamic match_Time;
@dynamic match_Location;
@dynamic match_Toss;
@dynamic match_PoM;
@dynamic match_Type;
@dynamic match_Overs;
@dynamic match_InningsCount;
@dynamic match_Result;
@dynamic match_Status;
@dynamic fromUmpire;
@dynamic fromInnings;
@dynamic fromTeam;

@end
