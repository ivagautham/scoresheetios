//
//  TeamRuns.m
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import "TeamRuns.h"
#import "Team.h"


@implementation TeamRuns

@dynamic teamRuns_TotalRuns;
@dynamic teamRuns_Extras;
@dynamic toTeam;

@end
