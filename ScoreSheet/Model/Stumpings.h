//
//  Stumpings.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Player;

@interface Stumpings : NSManagedObject

@property (nonatomic, retain) NSNumber * stumpings_TotalStumpings;
@property (nonatomic, retain) Player *toPlayer;

@end
