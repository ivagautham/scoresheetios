//
//  TeamCluster.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TeamCluster : NSManagedObject

@property (nonatomic, retain) NSNumber * teamCluster_Over;
@property (nonatomic, retain) NSNumber * teamCluster_Ball;
@property (nonatomic, retain) NSString * teamCluster_Result;
@property (nonatomic, retain) NSManagedObject *toTeam;

@end
