//
//  Match.h
//  ScoreSheet
//
//  Created by Gautham on 11/04/14.
//  Copyright (c) 2014 q98. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Innings, Umpire;

@interface Match : NSManagedObject

@property (nonatomic, retain) NSDate * match_Time;
@property (nonatomic, retain) NSString * match_Location;
@property (nonatomic, retain) NSString * match_Toss;
@property (nonatomic, retain) NSString * match_PoM;
@property (nonatomic, retain) NSNumber * match_Type;
@property (nonatomic, retain) NSNumber * match_Overs;
@property (nonatomic, retain) NSNumber * match_InningsCount;
@property (nonatomic, retain) NSNumber * match_Result;
@property (nonatomic, retain) NSNumber * match_Status;
@property (nonatomic, retain) NSSet *fromUmpire;
@property (nonatomic, retain) NSSet *fromInnings;
@property (nonatomic, retain) NSSet *fromTeam;
@end

@interface Match (CoreDataGeneratedAccessors)

- (void)addFromUmpireObject:(Umpire *)value;
- (void)removeFromUmpireObject:(Umpire *)value;
- (void)addFromUmpire:(NSSet *)values;
- (void)removeFromUmpire:(NSSet *)values;

- (void)addFromInningsObject:(Innings *)value;
- (void)removeFromInningsObject:(Innings *)value;
- (void)addFromInnings:(NSSet *)values;
- (void)removeFromInnings:(NSSet *)values;

- (void)addFromTeamObject:(NSManagedObject *)value;
- (void)removeFromTeamObject:(NSManagedObject *)value;
- (void)addFromTeam:(NSSet *)values;
- (void)removeFromTeam:(NSSet *)values;

@end
